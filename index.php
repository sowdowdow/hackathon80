<?php
  session_start();
  require_once 'assets/php/head.php';

  $carouselColors = array('red', 'amber', 'green', 'blue');
  srand(time());
  $randColorIndex = array_rand($carouselColors);

  $icons = array('directions_car', 'account_circle','announcement');


  $database = new PDO('mysql:dbname=hackset;port=3306;charset=UTF8;host=localhost', 'root', 'root');

  $stmt = $database->query("SELECT * FROM Annonce LIMIT 5");
  if (empty($stmt)) {
      echo 'DB ERROR';
      exit;
  }
  $nbEnregistrements = $stmt->rowCount();
  $annonces = $stmt->fetchAll(PDO::FETCH_ASSOC);

  // var_dump($annonces);
  foreach ($annonces as $key => $annonce) {
      $stmt = $database->query("SELECT * FROM Individu WHERE idIndividu = ".$annonce["publicateur"]);
      $publicateurs[$key] = $stmt->fetch();
  }

?>


  <body>
    <?php require_once 'assets/php/nav.php'; ?>
    <main>
      <div class="row py1 opacgrey" style="margin-bottom: 0; padding-top: 0; padding-bottom: 0">
          <a href="#">
            <div class="col s12 m4">
              <div class="card">
                <div class="card-image">
                  <img src="assets/images/MaResidence.jpg">
                  <span class="card-title shadowText">Ma résidence</span>
                </div>
              </div>
            </div>
          </a>

          <a href="#">
            <div class="col s12 m4">
              <div class="card">
                <div class="card-image">
                  <img src="assets\images\MonQuartier.jpg">
                  <span class="card-title shadowText">Mon Quartier</span>
                </div>
              </div>
            </div>
          </a>

          <a href="#">
            <div class="col s12 m4">
              <div class="card">
                <div class="card-image">
                  <img src="assets\images\MaVille.jpg">
                  <span class="card-title shadowText">Ma Ville</span>
                </div>
              </div>
            </div>
          </a>


      </div>
      <div class="row" style="display: flex; margin-bottom: 0px;">
        <div class="col s6 m8 l9">
          <!-- liste des actus -->
          <?php foreach ($annonces as $key => $annonce): ?>
            <?php
              $date = new DateTime($annonce["datePublication"]);
             ?>
            <div class="row" id="<?php echo 'row'.$key ?>" style="display: flex;">
              <div class="col l2 center-align hide-on-med-and-down" style="margin: auto;">
                <i class="material-icons large"><?= (!empty($annonce["icon"]) ? $annonce["icon"] : 'account_circle') ?></i>
              </div>
              <div class="col m12 <?= (isset($_GET['gardien'])) ? 'l8' : 'l10' ?>" id="<?php echo 'collen'.$key ?>">
                <div class="card">
                  <div class="card-content">
                    <div class="card-title">
                      <?= $annonce['nom'] ?>
                    </div>
                    <div class="card-stacked">
                      <div>
                        <p class="opacgreyText">De <?= $publicateurs[$key]['nom'] ?>, à <?= $publicateurs[$key]['commune'] ?><span class="right">le <?php echo $date->format('d/m/Y à H:i'); ?></span></p>
                      </div>
                      <div>
                        <p><?= $annonce['message'] ?></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <?php if (isset($_GET['gardien'])): ?>
                <div class="col l2 center-align" style="margin: auto;" id="<?php echo 'buttonCol'.$key ?>">
                  <div class="pb1">
                    <button class="validateButton waves-effect waves-light btn-small green" id="<?php echo 'validate'.$key ?>" style="width: 100%"><i class="material-icons left">check</i>Accepter</button>
                  </div>
                  <div>
                    <button class="deletingButton waves-effect waves-light btn-small red" id="<?php echo 'delete'.$key ?>" style="width: 100%"><i class="material-icons left">close</i>Rejeter</button>
                  </div>
                </div>
              <?php endif; ?>

            </div>
          <?php endforeach; ?>
          <!-- fin liste actcus -->
        </div>
        <?php include_once 'assets/php/navRight.php'; ?>
      </div>
    </main>

    <?php
      require_once 'assets/php/footer.php';
      require_once 'assets/php/scripts.php';
    ?>

  </body>

  </html>
