<?php
  session_start();
  require_once 'assets/php/head.php';

  $carouselColors = array('red', 'green', 'blue');
  srand(time());
  $randColorIndex = array_rand($carouselColors);
  $database = new PDO('mysql:host=localhost;port=3306;dbname=hackset', 'root', 'root');

  $stmt = $database->query("SELECT * FROM Annonce ORDER BY idAnnonce DESC");
  $nbEnregistrements = $stmt->rowCount();
  $annonces = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_ABS, $nbEnregistrements);


  srand(time(null));
  $stmt = $database->query("SELECT * FROM Individu WHERE idIndividu = ".$annonces["publicateur"]);
  $publicateurs = $stmt->fetch();
  $stmt = $database->query("SELECT * FROM Individu WHERE idIndividu = ".$annonces["publicateur"]);
  $publicateurs = $stmt->fetch();

?>

  <body>
    <head>
    </head>
    <main>
      <div class="carousel-item <?= $carouselColors[$randColorIndex] ?> white-text" href="#two!">
        <div class="center-align announce">
            <div class="colorfill A<?php echo $carouselColors[$randColorIndex] ?>">

            </div>
            <div class="infos A<?php echo $carouselColors[$randColorIndex] ?>">
              <h2 class="s3"><?php echo "Annonce" ?></h2>
              <p class="s2">de : <?php echo $publicateurs["prenom"]." ".$publicateurs["nom"]; ?>, le <?php echo $annonces["datePublication"]; ?></p>
            </div>
            <div class="title A<?php echo $carouselColors[$randColorIndex] ?>">
              <h2 class="s4"><strong><?php echo $annonces["nom"]; ?></strong></h2>
            </div>
          <div class="message">
            <h1><?php echo $annonces["message"]; ?></h1>
          </div>
          <div class="qr">
            <?php
            if ($annonces["lienTierce"] == null) {
            } else {
                ?>
              <img src="assets/php/qr.png.php?image=<?php echo $annonces["lienTierce"]; ?>&pixel=8&frame=2" width=200em>;
            <?php
            }
            ?>
          </div>
        </div>
      </div>
    </main>

    <?php
      require_once 'assets/php/scripts.php';
    ?>

    <script type="text/javascript">
      setInterval(function(){
        location.reload();
      },5000)
    </script>

  </body>

  </html>
