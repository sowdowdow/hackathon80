<!DOCTYPE html>


<?php
  session_start();
  require_once 'assets/php/head.php';
?>
<head>
    <meta charset="utf-8">
</head>
  <body>
    <?php require_once 'assets/php/nav.php'; ?>
    <main>
      <div class="row" style="display: flex; height: 100%">
        <div class="col s6 m8 l9">
          <div class="container">
            <h3>Déposer une annonce</h3>
            <form action="ajoutAnnonce.php">
              <div class="input-field">
                <div class="col s12">
                  <label for = "Titre">Titre de l'annonce :</label>
                  <input id="Titre" name="nom">
                </div>
                <div class="col s12 m6 input-field">
                  <i class="material-icons prefix">list</i>
                  <select id="Type" name="type">
                    <option value="" disabled selected>Type d'annonce :</option>
                    <option value="0">Vente, don, prêt d'Objets</option>
                    <option value="1">Covoiturage</option>
                    <option value="2">Service</option>
                    <option value="3">Événement culturel</option>
                    <option value="4">Info pratique</option>
                  </select>
                </div>
                <div class="col s12 m6 input-field"><i class="material-icons prefix">settings_input_antenna</i>
                  <select name="portee" id="Portee">
                    <option value="" disabled selected>Diffuser dans :</option>
                    <option value="maResidence">Ma résidence</option>
                    <option value="monQuartier">Mon quartier</option>
                    <option value="maVille">Ma ville</option>
                  </select>
                </div>
                <div class="row">
                  <div class="col s12">
                    <div class="col s12">
                      <label for="Message">Message de l'annonce :</label>
                      <textarea name="message" id="Message" cols="30" rows="10" class="materialize-textarea"></textarea>
                    </div>
                    <div class="col s12">
                      <label for="Lien">Lien vers une annonce externe :</label>
                      <input type="text" name = "lien" id = "Lien">
                    </div>
                  </div>
                  <div class="col s12 center-align my1">
                    <button class="btn waves-effect waves-light large-btn" type="submit" name="action">Envoyer
                      <i class="material-icons right">send</i>
                    </button>
                  </div>
              </div>
            </form>
            </div>
          </div>
      </div>
      <?php include_once 'assets/php/navRight.php'; ?>
    </div>
  </main>

  <?php
    require_once 'assets/php/footer.php';
    require_once 'assets/php/scripts.php';
  ?>

  </body>
</html>
