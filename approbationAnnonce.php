<?php
  session_start();
  require_once 'assets/php/head.php';
?>


  <body>
    <?php require_once 'assets/php/nav.php'; ?>
    <main>
      <div class="row height100">
        <div class="col s6 m8 l9">
          <div class="row py1 opacgrey">
            <div class="chip">Mon Quartier<i class="close material-icons">close</i>
            </div>
            <div class="chip">Tag<i class="close material-icons">close</i>
            </div>
            <div class="chip">Tag<i class="close material-icons">close</i>
            </div>
          </div>

          <div class="col s6 m8 l10">
            <div class="card horizontal">

              <div class="col s4 m6 l8">
                <div class="card horizontal">
                  <div class="card-image">
                    <i class="material-icons large">warning</i>
                  </div>
                  <div class="card-stacked">
                    <div class="card-content">
                      <p>Travaux sur voiries</p>
                    </div>
                    <div class="col s12 m2">
                      <p class="right">De Amiens métropole</p>
                      <i class="material-icons">directions_car</i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="center-align">
                <button class="waves-effect waves-light btn-large"><i class="material-icons left">check</i>Accepter</button>
              </div>
              <div class="center-align">
                <button class="waves-effect waves-light btn-large"><i class="material-icons left">close</i>Rejeter</button>
              </div>

            </div>
          </div>

        </div>
        <?php include_once 'assets/php/navRight.php'; ?>
      </div>
    </main>

    <?php
      require_once 'assets/php/footer.php';
      require_once 'assets/php/scripts.php';
    ?>

  </body>

  </html>
