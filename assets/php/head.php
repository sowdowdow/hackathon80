<!DOCTYPE html>
<html lang="fr">

<head>
  <meta name="author" content="Simon Dormeau, Corentin Lesage, Corentin Lesage, Kaïs Hadi, Vivien Cantelou, Rémi Lefévre, Yoann Legrand">
  <meta name="description" content="Epreuve de web des 24H des IUT - 2018">
  <meta name="keywords" content="24H des IUTS, Clair .Net et Précis">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0" />
  <title>hackathon80</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="assets/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection" />
  <link href="assets/css/master.css" type="text/css" rel="stylesheet" media="screen,projection" />
  <?php if (basename($_SERVER['REQUEST_URI'], '.php') == 'globalDisplay'): ?>
    <link href="assets/css/global.css" type="text/css" rel="stylesheet" media="screen,projection" />
  <?php endif; ?>
</head>
