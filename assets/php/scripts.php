<!--  Scripts-->
<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/js/materialize.min.js"></script>
<script src="assets/js/init.js"></script>
<script type="text/javascript">
  //on initialise le datepicker
  $(document).ready(function(){
    M.AutoInit();
    M.updateTextFields();
    $('.datepicker').datepicker();
    $('select').formSelect();


    //au clique on supprime l'annonce (fake)
    $('.deletingButton').click(function () {
      var idToDelete = '#row'+this.id.toString().replace('delete', '');
      $(idToDelete).hide(200);
      //toasts
      var toastHTML = '<span>Annonce supprimée</span><button class="btn-flat toast-action" id="toast'+this.id.toString().replace('delete', '')+'">Annuler</button>';
      M.toast({html: toastHTML});
    });
    //validation
    $('.validateButton').click(function () {
      //on cache les boutons
      var idToDelete = '#buttonCol'+this.id.toString().replace('validate', '');
      var colToExpand = '#collen'+this.id.toString().replace('validate', '');
      $(colToExpand).toggleClass('l8');
      $(colToExpand).toggleClass('l10');
      $(idToDelete).hide(200);
      console.log(idToDelete);
      //toasts
      var toastHTML = '<span>Annonce validée</span><button class="btn-flat toast-action" id="toast'+this.id.toString().replace('delete', '')+'">Annuler</button>';
      M.toast({html: toastHTML});
    });

});
</script>

<?php if (basename($_SERVER['REQUEST_URI'], '.php') == 'globalDisplay'): ?>
  <script type="text/javascript">
    // quand le document est prêt
    $(document).ready(function(){

      //on initialise le carousel
      $('.carousel.carousel-slider').carousel({
        indicators: true,
      });

      //cette fonction récupère le nouvel évènement et l'ajoute au carousel
      function update() {
        $.ajax({
          method: "post",
          url: "assets/php/globalAnnounceGenerator.php",
          data: { name: "John", location: "Boston" }
        }).done(function( msg ) {
          // console.log(msg);
          $('#carousel').append(msg);
          M.Carousel.getInstance($('.carousel')).destroy();
          $('.carousel.carousel-slider').carousel({
            indicators: true,
          });
        }).fail(function (msg) {
          console.log('fail');
          console.log(msg);
        });
      }

      //toute les 7 secondes, on change de slide et on va chercher le dernier évènement
      setInterval(function () {
        M.Carousel.getInstance($('.carousel')).next();
        update();
      }, 7000);
    });
</script>
<?php endif; ?>
