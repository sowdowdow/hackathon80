<?php
  //on crée une couleur aléatoire
  $carouselColors = array('red', 'amber', 'green', 'blue');
  srand(time());
  $randColorIndex = array_rand($carouselColors);
  $database = new PDO('mysql:host=localhost;port=3306;dbname=hackset','root','root');

  $stmt = $database->query("SELECT * FROM Annonce");
  $nbEnregistrements = $stmt->rowCount();
  if($nbEnregistrements > 5)
  {
    $annonces[0] = $stmt->fetch(PDO::FETCH_ASSOC,PDO::FETCH_ORI_ABS,$nbEnregistrements-5);
    $annonces[1] = $stmt->fetch(PDO::FETCH_ASSOC,PDO::FETCH_ORI_ABS,$nbEnregistrements-4);
    $annonces[2] = $stmt->fetch(PDO::FETCH_ASSOC,PDO::FETCH_ORI_ABS,$nbEnregistrements-3);
    $annonces[3] = $stmt->fetch(PDO::FETCH_ASSOC,PDO::FETCH_ORI_ABS,$nbEnregistrements-2);
    $annonces[4] = $stmt->fetch(PDO::FETCH_ASSOC,PDO::FETCH_ORI_ABS,$nbEnregistrements-1);
    $annonces[5] = $stmt->fetch(PDO::FETCH_ASSOC,PDO::FETCH_ORI_ABS,$nbEnregistrements-0);
  }
  else {
    $annonces = $stmt->fetchAll(PDO::FETCH_ASSOC);
  }
  srand(time(NULL));
  $randAnnonce = (rand() % (count($annonces) - 1));
  $stmt = $database->query("SELECT * FROM Individu WHERE idIndividu = ".$annonces[$randAnnonce]["publicateur"]);
  $publicateur = $stmt->fetch();

 ?>
 <!-- cette page génère un slide pour le carrousel -->
<div class="carousel-item <?= $carouselColors[$randColorIndex] ?> white-text" href="#two!">
  <div class="center-align announce">
    <div class="infos">
      <h2 class="s2"><?php print_r($annonces[$randAnnonce]); echo $randAnnonce; ?></h2>
      <p class="s3">de : <?php echo $publicateur["nom"]; ?>, le <?php echo $annonces[$randAnnonce]["datePublication"]  ?> </p>
      <p class="s3">Habite au : <?= rand(0, 5) ?><sup>ème</sup> étage</p>
    </div>
    <div class="title">
      <h2 class="s4 poiret"><?php echo $annonces[$randAnnonce]["nom"]; ?></h2>
    </div>
    <div class="message">
      <h1><?php echo $annonces[$randAnnonce]["message"]; ?></h1>
    </div>
    <div class="qr">
      <?php
      if(empty($lien))
      {}
      else {

        echo "<img src=\"assets/php/qr.png.php?image=\"".$annonces[$randAnnonce]["lienTierce"]."?>&pixel=8&frame=2\" alt=\"QRCode\" width=\"200em\">";
      }
      ?>
    </div>
  </div>
</div>
