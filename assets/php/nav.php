<header>
  <nav class="white" role="navigation">
    <!-- classic navbar -->
    <div class="nav-wrapper">
      <img src="assets/images/LogoWeb.jpg" alt="logo" height="64px">
      <a id="logo-container" href="index.php" class="brand-logo opacgreyText" style="padding-left: 50px;"><i id="presdechezmoi">“Près de chez moi...”</i></a>
      <ul class="right hide-on-med-and-down">
        <li>
          <?php if (isset($_GET['gardien'])): ?>
            <a class="opacgreyText"><i class="material-icons right">exit_to_app</i>Bonjour Roger R.</a>
          <?php endif; ?>
          <?php if (isset($_GET['user'])): ?>
            <a class="opacgreyText"><i class="material-icons right">exit_to_app</i>Bonjour Amandine S.</a>
          <?php endif; ?>
          </a>
        </li>
      </ul>

      <!-- mobile sidenav -->
      <ul id="nav-mobile" class="sidenav opacgreyText">
        <li>
          <?php if (isset($_GET['gardien'])): ?>
            <a class="opacgreyText"><i class="material-icons right">exit_to_app</i>Bonjour Roger R.</a>
          <?php endif; ?>
          </a>
        </li>
      </ul>
      <a href="#" data-target="nav-mobile" class="sidenav-trigger opacgreyText"><i class="material-icons">menu</i></a>
    </div>
  </nav>
</header>
