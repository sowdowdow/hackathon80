<?php
if (!empty($_GET['image'])) {
    require_once __DIR__.'/phpqrcode/qrlib.php';
    $text = $_GET['image'];
    $size = 3;
    $frame = 4;

    if (!empty($_GET['pixel'])) {
        $size = $_GET['pixel'];
    }
    if (!empty($_GET['frame'])) {
        $frame = $_GET['frame'];
    }

    QRcode::png($text, false, $level = QR_ECLEVEL_L, $size, $frame, false);
}
