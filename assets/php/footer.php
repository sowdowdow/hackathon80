<footer class="page-footer opacgrey">
  <div class="container">
    <div class="row">
      <div class="col l9 s12">
        <h5 class="white-text">OPAC Amiens</h5>
        <p>Réalisé en deux jours par trois étudiants sous caféine.</p>


      </div>
      <div class="col l3 s12">
        <h5 class="white-text">Settings</h5>
        <ul>
          <li><a href="globalDisplay.php">Affichage écran</a></li>
          <li><a href="#">Nous contacter</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="footer-copyright">
    <div class="container">
      <div id="copyright">Copyright © 2018 <a href="/">OPH D'AMIENS</a> | <a href="/?q=node/132">Mentions Légales</a></div>
    </div>
  </div>
</footer>
